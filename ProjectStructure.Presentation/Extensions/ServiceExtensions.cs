﻿using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.BLL.Contracts;
using ProjectStructure.BLL.Repositories;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.Presentation.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<ITaskRepository, TaskRepository>();
            services.AddScoped<ITeamRepository, TeamRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<UserService>();
            services.AddScoped<TeamService>();
            services.AddScoped<ProjectService>();
            services.AddScoped<TaskService>();
        }
    }
}
