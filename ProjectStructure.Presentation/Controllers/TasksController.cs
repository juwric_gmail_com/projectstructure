﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.DTOs.Task;
using System.Collections.Generic;

namespace ProjectStructure.Presentation.Controllers
{
    public class TasksController : BaseController
    {
        private readonly TaskService _taskService;

        public TasksController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public ActionResult<List<TaskDTO>> Get()
        {
            return Ok(_taskService.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> GetById(int id)
        {
            return Ok(_taskService.GetTaskById(id));
        }

        [HttpPut]
        public IActionResult UpdateTask([FromBody] TaskUpdateDTO task)
        {
            _taskService.UpdateTask(task);
            return NoContent();
        }

        [HttpPost]
        public IActionResult CreateUser([FromBody] TaskCreateDTO task)
        {
            _taskService.CreateTask(task);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _taskService.DeleteTask(id);
            return NoContent();
        }
    }
}
