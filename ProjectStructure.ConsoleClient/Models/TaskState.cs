﻿namespace CollectionsLINQ.Models
{
    public enum TaskState
    {
        Finished,
        UnFinished,
        Canceled,
        InProcess
    }
}
