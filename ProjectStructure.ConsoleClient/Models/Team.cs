﻿using System;

namespace CollectionsLINQ.Models
{
    public class Team : Base
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
