﻿using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.Common.DTOs.Team
{
    public class TeamCreateDTO
    {
        [Required]
        public string Name { get; set; }
    }
}
