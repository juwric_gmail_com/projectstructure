﻿using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.Common.DTOs.Team
{
    public class TeamUpdateDTO
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
