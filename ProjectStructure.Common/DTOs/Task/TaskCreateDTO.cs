﻿using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.Common.DTOs.Task
{
    public class TaskCreateDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }

        [Required]
        public int ProjectId { get; set; }
        [Required]
        public int PerformerId { get; set; }
    }
}
