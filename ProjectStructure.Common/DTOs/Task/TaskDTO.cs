﻿using ProjectStructure.Common.Enums;
using System;

namespace ProjectStructure.Common.DTOs.Task
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
