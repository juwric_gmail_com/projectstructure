﻿using CollectionsLINQ;
using CollectionsLINQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.Client
{
    internal class Program
    {
        static IEnumerable<Project> list;

        public static void Main(string[] args)
        {
            MappingService service = new MappingService("https://localhost:5001");
            service.Init().Wait();
            list = service.GetProjectList();

            var req1 = GetTasksSpecificUser();
            var req2 = GetTasksAssigned();
            var req3 = GetListTasksCompleted();
            var req4 = GetListWihtIdTeamName();
            var req5 = GetListOfUsersAlphabetically();
            var req6 = GetStructure1();
            var req7 = GetStructure2();
        }

        public static Dictionary<Project, int> GetTasksSpecificUser(int userId = 29)
        {
            return list.Where(q => q.AuthorId == userId)
                .ToDictionary(p => p, t => t.Tasks.Count());
        }

        public static List<Task> GetTasksAssigned(int userId = 34)
        {
            var result = list.SelectMany(p => p.Tasks)
                .Where(t => t.PerformerId == userId && t.Name.Length < 45);

            return result.ToList();
        }

        public static List<dynamic> GetListTasksCompleted(int userId = 34)
        {
            var result = list.SelectMany(p => p.Tasks)
                .Where(t => t.PerformerId == userId && t.State == TaskState.Finished && t.FinishedAt?.Year == 2021)
                .Select(t => new { id = t.Id, name = t.Name });

            return result.ToList<dynamic>();
        }


        public static List<dynamic> GetListWihtIdTeamName()
        {
            int year = DateTime.Now.Year;
            var result = MappingService.teams.GroupJoin(
               MappingService.users,
                team => team.Id,
                user => user.TeamId,
                (team, user) => new
                {
                    id = team.Id,
                    name = team.Name,
                    users = user
                    .OrderByDescending(u => u.RegisteredAt)
                    .Where(u => (year - u.BirthDay.Year) > 10)
                });

            return result.ToList<dynamic>();
        }

        public static List<dynamic> GetListOfUsersAlphabetically()
        {
            var result = MappingService.users.OrderBy(u => u.FirstName).GroupJoin(
                MappingService.tasks,
                user => user.Id,
                task => task.PerformerId,
                (user, task) => new
                {
                    first_name = user.FirstName,
                    tasks = task.OrderByDescending(t => t.Name.Length)
                });

            return result.ToList<dynamic>();
        }

        public static List<dynamic> GetStructure1(int userId = 34)
        {
            var result = MappingService.users.Where(u => u.Id == userId).GroupJoin(MappingService.projects,
                u => u.Id,
                p => p.AuthorId,
                (u, p) => new
                {
                    user = u,
                    lastProject = p.OrderBy(lp => lp.CreatedAt).First(),
                }).GroupJoin(MappingService.tasks,
                    prev => prev.user.Id,
                    t => t.PerformerId,
                    (prev, t) => new
                    {
                        user = prev.user,
                        lastProject = prev.lastProject,
                        totalUnFinishedOrCanceled = t.Where(t =>
                            t.State == TaskState.UnFinished || t.State == TaskState.Canceled),
                        longestTask = t.OrderByDescending(t => t.FinishedAt - t.CreatedAt).First()
                    });

            return result.ToList<dynamic>();
        }

        public static List<dynamic> GetStructure2()
        {
            var result = MappingService.projects.Select(p => new
            {
                project = p,
                longTask = p.Tasks.OrderByDescending(t => t.Description.Length).First(),
                shortTask = p.Tasks.OrderBy(t => t.Name.Length).First(),
            });

            return null;
        }
    }
}
