﻿using Newtonsoft.Json;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.IO;

namespace ProjectStructure.BLL
{
    public static class DbInitializer
    {
        public static void Initialize()
        {
            AppDbContext.Users = Init<User>(@"..\ProjectStructure.DAL\Seeds\Users.json");
            AppDbContext.Tasks = Init<Task>(@"..\ProjectStructure.DAL\Seeds\Tasks.json");
            AppDbContext.Teams = Init<Team>(@"..\ProjectStructure.DAL\Seeds\Teams.json");
            AppDbContext.Projects = Init<Project>(@"..\ProjectStructure.DAL\Seeds\Projects.json");
        }

        private static List<T> Init<T>(string path)
        {
            var items = new List<T>();
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                items = JsonConvert.DeserializeObject<List<T>>(json);
            }

            return items;
        }
    }
}
