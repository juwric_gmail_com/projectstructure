﻿using AutoMapper;
using ProjectStructure.Common.DTOs.Team;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamCreateDTO, Team>();
            CreateMap<TeamUpdateDTO, Team>();
        }
    }
}
