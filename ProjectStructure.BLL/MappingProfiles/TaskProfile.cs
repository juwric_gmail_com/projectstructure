﻿using AutoMapper;
using ProjectStructure.Common.DTOs.Task;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<TaskCreateDTO, Task>();
            CreateMap<TaskUpdateDTO, Task>();
        }
    }
}
