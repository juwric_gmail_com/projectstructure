﻿using ProjectStructure.BLL.Contracts;
using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private readonly List<T> _list;

        public BaseRepository(List<T> list)
        {
            _list = list;
        }

        public List<T> GetAll()
        {
            return _list;
        }

        public T GetById(int id)
        {
            return _list.FirstOrDefault(e => e.Id == id);
        }

        public void Add(T entity)
        {
            _list.Add(entity);
        }

        public void Delete(int id)
        {
            var index = _list.FindIndex(e => e.Id == id);
            if (index >= 0)
            {
                _list.RemoveAt(index);
            }
        }

        public void Update(T entity)
        {
            var index = _list.FindIndex(e => e.Id == entity.Id);
            if (index >= 0)
            {
                _list[index] = entity;
            }
        }
    }
}
