﻿using AutoMapper;
using ProjectStructure.BLL.Contracts;
using ProjectStructure.Common.DTOs.Team;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class TeamService : BaseService
    {
        private readonly ITeamRepository _teamRepository;

        public TeamService(IMapper mapper, ITeamRepository teamRepository) : base(mapper)
        {
            _teamRepository = teamRepository;
        }

        public List<TeamDTO> GetAll()
        {
            var teams = _teamRepository.GetAll();
            return _mapper.Map<List<TeamDTO>>(teams);
        }

        public TeamDTO GetUserById(int id)
        {
            var user = _teamRepository.GetById(id);
            if (user == null)
            {
                throw new Exception("The team is not found");
            }

            return _mapper.Map<TeamDTO>(user);
        }

        public TeamDTO CreateUser(TeamCreateDTO teamCreateDTO)
        {
            var teamEntity = _mapper.Map<Team>(teamCreateDTO);
            teamEntity.Id = (_teamRepository.GetAll().Max(u => u.Id)) + 1;
            teamEntity.CreatedAt = DateTime.Now;
            _teamRepository.Add(teamEntity);

            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public void UpdateUser(TeamUpdateDTO teamUpdateDTO)
        {
            var teamEntity = GetUserById(teamUpdateDTO.Id);
            var updatedTeamEntity = _mapper.Map<Team>(teamUpdateDTO);
            updatedTeamEntity.CreatedAt = teamEntity.CreatedAt;

            _teamRepository.Update(updatedTeamEntity);
        }

        public void DeleteUser(int id)
        {
            GetUserById(id);
            _teamRepository.Delete(id);
        }
    }
}
