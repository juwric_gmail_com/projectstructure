﻿using AutoMapper;
using ProjectStructure.BLL.Contracts;
using ProjectStructure.Common.DTOs.Task;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class TaskService : BaseService
    {
        private readonly ITaskRepository _taskRepository;

        public TaskService(IMapper mapper, ITaskRepository taskRepository) : base(mapper)
        {
            _taskRepository = taskRepository;
        }

        public List<TaskDTO> GetAll()
        {
            var tasks = _taskRepository.GetAll();
            return _mapper.Map<List<TaskDTO>>(tasks);
        }

        public TaskDTO GetTaskById(int id)
        {
            var task = _taskRepository.GetById(id);
            if (task == null)
            {
                throw new Exception("The task is not found");
            }

            return _mapper.Map<TaskDTO>(task);
        }

        public TaskDTO CreateTask(TaskCreateDTO taskCreateDTO)
        {
            var taskEntity = _mapper.Map<Task>(taskCreateDTO);
            taskEntity.Id = (_taskRepository.GetAll().Max(u => u.Id)) + 1;
            taskEntity.CreatedAt = DateTime.Now;
            _taskRepository.Add(taskEntity);

            return _mapper.Map<TaskDTO>(taskEntity);
        }

        public void UpdateTask(TaskUpdateDTO taskUpdateDTO)
        {
            var entity = GetTaskById(taskUpdateDTO.Id);
            var updatedEntity = _mapper.Map<Task>(taskUpdateDTO);
            updatedEntity.CreatedAt = entity.CreatedAt;

            _taskRepository.Update(updatedEntity);
        }

        public void DeleteTask(int id)
        {
            GetTaskById(id);
            _taskRepository.Delete(id);
        }
    }
}
