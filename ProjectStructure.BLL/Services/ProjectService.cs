﻿using AutoMapper;
using ProjectStructure.BLL.Contracts;
using ProjectStructure.Common.DTOs.Project;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class ProjectService : BaseService
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectService(IMapper mapper, IProjectRepository projectRepository) : base(mapper)
        {
            _projectRepository = projectRepository;
        }

        public List<ProjectDTO> GetAll()
        {
            var teams = _projectRepository.GetAll();
            return _mapper.Map<List<ProjectDTO>>(teams);
        }

        public ProjectDTO GetProjectById(int id)
        {
            var user = _projectRepository.GetById(id);
            if (user == null)
            {
                throw new Exception("The project is not found");
            }

            return _mapper.Map<ProjectDTO>(user);
        }

        public ProjectDTO CreateProject(ProjectCreateDTO projectCreateDTO)
        {
            var teamEntity = _mapper.Map<Project>(projectCreateDTO);
            teamEntity.Id = (_projectRepository.GetAll().Max(u => u.Id)) + 1;
            teamEntity.CreatedAt = DateTime.Now;
            _projectRepository.Add(teamEntity);

            return _mapper.Map<ProjectDTO>(teamEntity);
        }

        public void UpdateProject(ProjectUpdateDTO projectUpdateDTO)
        {
            var projectEntity = GetProjectById(projectUpdateDTO.Id);
            var updatedProjectEntity = _mapper.Map<Project>(projectUpdateDTO);
            updatedProjectEntity.CreatedAt = projectEntity.CreatedAt;
            updatedProjectEntity.Deadline = projectEntity.Deadline;

            _projectRepository.Update(updatedProjectEntity);
        }

        public void DeleteProject(int id)
        {
            GetProjectById(id);
            _projectRepository.Delete(id);
        }
    }
}
