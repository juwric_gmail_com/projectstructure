﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;

namespace ProjectStructure.DAL
{
    public static class AppDbContext
    {
        public static List<Project> Projects { get; set; }
        public static List<Task> Tasks { get; set; }
        public static List<Team> Teams { get; set; }
        public static List<User> Users { get; set; }
    }
}
