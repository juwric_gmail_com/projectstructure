﻿namespace ProjectStructure.DAL.Models
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
