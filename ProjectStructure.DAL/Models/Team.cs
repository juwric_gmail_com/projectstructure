﻿using System;

namespace ProjectStructure.DAL.Models
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
