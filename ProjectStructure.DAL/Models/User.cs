﻿using System;

namespace ProjectStructure.DAL.Models
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }
    }
}
